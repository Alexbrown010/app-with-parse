//
//  FirstViewController.m
//  Test
//
//  Created by Adam Hellewell on 6/18/15.
//  Copyright (c) 2015 Adam Hellewell. All rights reserved.
//

#import "FirstViewController.h"
#import <Parse/Parse.h>


@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UILabel* titleLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width,30)];
    titleLbl.text = [self.infoDictionary objectForKey:@"RecipeTitle"];
    titleLbl.textAlignment = NSTextAlignmentCenter;
    titleLbl.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:titleLbl];
    
    NSArray* widthConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[titleLbl(>=50)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titleLbl)];
    
    NSArray* heightConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[titleLbl(>=150)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titleLbl)];
    
    NSArray* xConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[titleLbl]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titleLbl)];
    
    NSArray* yConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[titleLbl]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titleLbl)];
    
    [self.view addConstraints:widthConstraint];
    [self.view addConstraints:heightConstraint];
    [self.view addConstraints:xConstraint];
    [self.view addConstraints:yConstraint];
    
    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 130, self.view.frame.size.width, 300)];
    iv.contentMode = UIViewContentModeScaleAspectFit;
    [iv setImage: [UIImage imageNamed: [self.infoDictionary objectForKey:@"Image"]]];
    iv.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:iv];
    
    NSArray* width1Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[iv(>=200)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(iv)];
    
    NSArray* height1Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[iv(>=150)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(iv)];
    
    NSArray* x1Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[iv]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(iv)];
    
    NSArray* y1Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[iv]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(iv)];
    
    [self.view addConstraints:width1Constraint];
    [self.view addConstraints:height1Constraint];
    [self.view addConstraints:x1Constraint];
    [self.view addConstraints:y1Constraint];
    
    UILabel* directionsTxt = [[UILabel alloc]initWithFrame:CGRectMake(0, 180, self.view.frame.size.width,self.view.frame.size.height)];
    directionsTxt.text = [self.infoDictionary objectForKey:@"RecipeDescription"];
    directionsTxt.textAlignment = NSTextAlignmentCenter;
    directionsTxt.lineBreakMode = NSLineBreakByWordWrapping;
    directionsTxt.numberOfLines = 0;
    directionsTxt.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:directionsTxt];
    
    NSArray* width2Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[directionsTxt(>=300)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(directionsTxt)];
    
    NSArray* height2Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[directionsTxt(>=100)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(directionsTxt)];
    
    NSArray* x2Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[directionsTxt]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(directionsTxt)];
    
    NSArray* y2Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-250-[directionsTxt]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(directionsTxt)];
    
    [self.view addConstraints:width2Constraint];
    [self.view addConstraints:height2Constraint];
    [self.view addConstraints:x2Constraint];
    [self.view addConstraints:y2Constraint];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
