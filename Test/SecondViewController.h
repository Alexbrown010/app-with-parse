//
//  SecondViewController.h
//  Test
//
//  Created by Adam Hellewell on 6/18/15.
//  Copyright (c) 2015 Adam Hellewell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController <UITextFieldDelegate> {
    NSString* recipeTitle;
    NSString* recipeDescription;
    
    UITextField *descriptionTxt;
    UITextField *titleTxt;

}

@property (nonatomic, retain)
NSArray *constraint_H;
@property (nonatomic, retain)
NSArray *constraint_V;
@property (nonatomic, retain)
NSDictionary *viewsDictionary;
@property (nonatomic, retain)
NSArray *arrayOfRecipies;
@property (nonatomic, retain)
UIAlertView* alert;

@end
