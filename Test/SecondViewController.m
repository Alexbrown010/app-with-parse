//
//  SecondViewController.m
//  Test
//
//  Created by Adam Hellewell on 6/18/15.
//  Copyright (c) 2015 Adam Hellewell. All rights reserved.
//

#import "SecondViewController.h"
#import <Parse/Parse.h>


@interface SecondViewController ()

@end

@implementation SecondViewController

UIGestureRecognizer *tapper;

//@synthesize titleTxt, descriptionTxt;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Allow for the keyboard to be put away
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
    self.view.backgroundColor = [UIColor purpleColor];
    
    //Submit button on the navigation controller
    UIBarButtonItem* btnAdd = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)];
    self.navigationItem.rightBarButtonItem = btnAdd;
  
//    UIScrollView *myScroll = [[UIScrollView alloc] init];
//    myScroll.frame = self.view.bounds; //scroll view occupies full parent view!
//    //specify CGRect bounds in place of self.view.bounds to make it as a portion of parent view!
//    myScroll.contentSize = CGSizeMake(400, 1200);   //scroll view size
//    myScroll.backgroundColor = [UIColor purpleColor];
//    myScroll.showsVerticalScrollIndicator = YES;
//    myScroll.showsHorizontalScrollIndicator = NO;
//    myScroll.scrollEnabled = YES;
//    [self.view addSubview:myScroll];
    
    UILabel* introLbl = [[UILabel alloc]init];
                         //WithFrame:CGRectMake(0, 100, self.view.frame.size.width, 30)];
    introLbl.text = @"Please enter in Recipe Title";
    introLbl.textAlignment = NSTextAlignmentCenter;
    introLbl.font = [UIFont boldSystemFontOfSize:16];
    introLbl.textColor = [UIColor whiteColor];
    introLbl.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:introLbl];
    
    //introLbl auto layout
    NSArray* widthConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[introLbl(>=50)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(introLbl)];
    
    NSArray* heightConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[introLbl(>=100)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(introLbl)];
    
    NSArray* xConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[introLbl]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(introLbl)];
    
    NSArray* yConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-30-[introLbl]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(introLbl)];
    
    [self.view addConstraints:widthConstraint];
    [self.view addConstraints:heightConstraint];
    [self.view addConstraints:xConstraint];
    [self.view addConstraints:yConstraint];
    
    titleTxt = [[UITextField alloc]init];
                             //WithFrame:CGRectMake(20, 140, self.view.frame.size.width - 40, 50)];
    titleTxt.backgroundColor = [UIColor whiteColor];
    titleTxt.placeholder = @"enter text";
    titleTxt.translatesAutoresizingMaskIntoConstraints = NO;
    titleTxt.delegate = self;
    [self.view addSubview:titleTxt];
    
    //titleTxt autolayout
    NSArray* width1Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[titleTxt(>=50)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titleTxt)];
    
    NSArray* height1Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[titleTxt(>=50)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titleTxt)];
    
    NSArray* x1Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[titleTxt]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titleTxt)];
    
    NSArray* y1Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[titleTxt]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titleTxt)];
    
    [self.view addConstraints:width1Constraint];
    [self.view addConstraints:height1Constraint];
    [self.view addConstraints:x1Constraint];
    [self.view addConstraints:y1Constraint];

    UILabel* descriptionLbl = [[UILabel alloc]init];
                               //WithFrame:CGRectMake(0, 200, self.view.frame.size.width, 30)];
    descriptionLbl.text = @"Please enter in Recipe Description";
    descriptionLbl.textAlignment = NSTextAlignmentCenter;
    descriptionLbl.font = [UIFont boldSystemFontOfSize:16];
    descriptionLbl.textColor = [UIColor whiteColor];
    descriptionLbl.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:descriptionLbl];
    
    //descriptionLbl autolayout
    NSArray* width2Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[descriptionLbl(>=50)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(descriptionLbl)];
    
    NSArray* height2Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[descriptionLbl(>=50)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(descriptionLbl)];
    
    NSArray* x2Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[descriptionLbl]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(descriptionLbl)];
    
    NSArray* y2Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-140-[descriptionLbl]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(descriptionLbl)];
    
    [self.view addConstraints:width2Constraint];
    [self.view addConstraints:height2Constraint];
    [self.view addConstraints:x2Constraint];
    [self.view addConstraints:y2Constraint];

    descriptionTxt = [[UITextField alloc]init];
                               //WithFrame:CGRectMake(20, 250, self.view.frame.size.width - 40, 400)];
    descriptionTxt.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
    descriptionTxt.backgroundColor = [UIColor whiteColor];
    descriptionTxt.placeholder = @"enter text";
    descriptionTxt.translatesAutoresizingMaskIntoConstraints = NO;
    descriptionTxt.delegate = self;
    [self.view addSubview:descriptionTxt];
    
    //descriptionTxt autolayout
    NSArray* width3Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[descriptionTxt(>=300)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(descriptionTxt)];
    
    NSArray* height3Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[descriptionTxt(>=100)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(descriptionTxt)];
    
    NSArray* x3Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[descriptionTxt]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(descriptionTxt)];
    
    NSArray* y3Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-180-[descriptionTxt]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(descriptionTxt)];
    
    [self.view addConstraints:width3Constraint];
    [self.view addConstraints:height3Constraint];
    [self.view addConstraints:x3Constraint];
    [self.view addConstraints:y3Constraint];
    
//    NSString *recipeTitle = titleTxt.text;
//    NSString *recipeDescription = descriptionTxt.text;
    
}

-(void)done:(id) sender {
    
    NSString *recipeTitle = titleTxt.text;
    NSString *recipeDescription = descriptionTxt.text;
    
    //NSLog(@"Here");
    
//    if (self.descriptionTxt.text.length > 0 && self.titleTxt.text.length > 0) {
        PFObject *recipe = [PFObject objectWithClassName:@"Recipe"];
        [recipe setObject:titleTxt.text forKey:@"recipeTitle"];
        [recipe setObject:descriptionTxt.text forKey:@"recipeDescription"];
//        recipe[@"recipeTitle"] = recipeTitle;
//        recipe[@"recipeDescription"] = recipeDescription;
        [recipe saveInBackground];
        
        
        [self.navigationController popViewControllerAnimated:YES];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                        message:@"Your recipe was submitted!."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        [self.navigationController popViewControllerAnimated:YES];

    
//    } else {
//        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Error"
//                                                        message:@"Your recipe was not saved"
//                                                       delegate:self
//                                              cancelButtonTitle:@"Ok"
//                                              otherButtonTitles: nil];
//        [alert1 show];
//        
//        [self.navigationController popViewControllerAnimated:YES];
//        
//    }

}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
