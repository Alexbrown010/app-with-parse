//
//  FirstViewController.h
//  Test
//
//  Created by Adam Hellewell on 6/18/15.
//  Copyright (c) 2015 Adam Hellewell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstViewController : UIViewController

@property (nonatomic, retain)
NSDictionary* infoDictionary;
@property (nonatomic, retain)
NSArray *constraint_H;
@property (nonatomic, retain)
NSArray *constraint_V;
@property (nonatomic, retain)
NSDictionary *viewsDictionary;

@end
