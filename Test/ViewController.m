//
//  ViewController.m
//  Test
//
//  Created by Adam Hellewell on 6/18/15.
//  Copyright (c) 2015 Adam Hellewell. All rights reserved.
//

#import "ViewController.h"
#import "FirstViewController.h"
#import "SecondViewController.h"
#import <Parse/Parse.h>


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrayOfRecipies = [NSArray new];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Recipe"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            arrayOfRecipies = objects;
            [tableView reloadData];
            //NSLog(@"Here");
        } else {
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
    
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"PList" ofType:@"plist"];
//    arrayOfRecipies = [[NSArray alloc]initWithContentsOfFile:filePath];
    
    tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    UIBarButtonItem *btnAdd = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addThis:)];
    self.navigationItem.rightBarButtonItem = btnAdd;
    
    UIBarButtonItem *btnLoad = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemPlay target:self action:@selector(loadThis:)];
    self.navigationItem.leftBarButtonItem = btnLoad;
    
}

-(void)addThis:(id)sender {
    SecondViewController* svc = [[SecondViewController alloc]init];
    [self.navigationController pushViewController:svc
                                         animated:YES];
}

-(void)loadThis:(id)sender {
    
    PFQuery *query = [PFQuery queryWithClassName:@"Recipe"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            arrayOfRecipies = objects;
            //NSLog(@"Here");
        } else {
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrayOfRecipies.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSDictionary* infoDictionary = arrayOfRecipies[indexPath.row];
    //NSLog(@"Here");
    cell.textLabel.text = [infoDictionary objectForKey:@"RecipeTitle"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary* infoDictionary = arrayOfRecipies[indexPath.row];
    
    
    FirstViewController* info = [FirstViewController new];
    info.infoDictionary = infoDictionary;
    [self.navigationController pushViewController:info animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
